# Informations complémentaires

## Prix libre et conscient

Nous avons calculé et décidé qu'il nous fallait chacun 1600 € /net par mois, ce qui avec les cotisations et les frais de fonctionnement revient peu ou prou à 3200 €.

En général, on facture 3 ou 4 jours par mois et le reste du temps on fait des trucs et on partage nos expériences avec des personnes et des organisations.

On imagine avoir quelques frais spécifiques à l'évènement comme les transports, et les repas collectifs par exemple. On imagine 350 € de frais, et comme d'habitude on publiera [la compta]()

Ce qui nous amène à penser que si on avait fixé un tarif, on serait partis sur un budget de 2400 € :

Soit :
Pour 5 ou 6 personnes : 400 € par /pers
Pour 10 personnes : 240 € par /pers

Pour :
[La semaine de 168h](https://www.youtube.com/watch?v=DgAs2ifekNo)

## Ateliers et fonctionnement

Si tu as déjà vécu un [forum ouvert](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert) tu ne seras sans doute pas trop dépaysé.

Par exemple, on suit la [loi universelle de mobilité](http://raphael.pierquin.com/blog/2009/10/une-organisation-regie-par-la-loi-des-2-pieds/) un peu comme on suit la Loi universelle de la gravitation.

### À quoi pourrait ressembler la journée ?

Il se pourrait que ça ressemble un peu à ça :

- deux trois moments où on se retrouve pour constituer une programmation et décrire l'intention des ateliers.
- des session collaboratives de 20' et de 45'.
- deux ou quatre espaces pour que les ateliers puissent se jouer en paralèlle.
- de temps en temps, un petit groupe de personne propose une montration qui nous permet de partager ce qui a été exploré.

- Pour le miam, on proposerait peut-être un buffet avec des produits frais tout au long de la journée.


### Et que ce passe-t-il dans les ateliers ? 

On utilise des formats que l'on a exploré dans d'autres évènements, et on en essaye d'autres.


    - <phorésie>
    - maub-code
    - pair-faire

- contrainte genre code retreat
    - pas de communication
    - coder comme dans les 60's

Exemples d'ateliers que l'on pourrait 


## Vous avez dit montages électroniques ?

Oui, on pense composants de base comme transistor, résistance, diodes, et autre leds mais aussi des systèmes plus complexes comme les micro controlleur et leur programmation.

Plus particulièrement, on viendra avec des [arduino](https://fr.wikipedia.org/wiki/Arduino) et des [ESP8266](https://fr.wikipedia.org/wiki/ESP8266), des capteurs en tout genre, des leds de toutes les couleurs et tout ce qui va bien pour faire les faire fonctionner ensemble.
