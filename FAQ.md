# Exploration électronique

Du matin au soir, on va apprendre ensemble à faire des montages électroniques utiles et fantaisistes.



# Ça se passe où, ça se passe quand ?

On se retrouve à Villeurbanne à la Myne à 5h le 15 juin 2018.

Pour lever l'ambiguité, quand on écrit 5h, c'est au matin que l'on pense.

On pense clôturer dans la soirée, un peu avant le passage au lendemain.

# Je pourrais avoir une idée du prix ?

On demande une Participation au Frais de 80 € à l'inscription.
Après l'évènement, on vous proposera de compléter d'un montant de votre choix, c'est ce qui fera notre rémunération.

Pour décider en conscience du prix nous te proposons [quelques pistes de réflexion]().

# Qui sommes nous ?

Antoine Vernois, je développe des logiciels, et je vous aide à fabriquer les votres.

Stéphane Langlois, j'aide les personnes et les organisations à concrétiser leurs idées.

# Quelle est votre [méthodologie](https://blog.crafting-labs.fr/2011/05/27/methodologie/) pédagogique ?

Blurp, en fait on n'est pas à l'aise avec l'idée de te former ou celle de t'éduquer, on préfère apprendre avec toi.

Cela fait pas mal de temps que nous revisitons le cadre des formations pour inventer des cadres d'apprentissage collaboratifs.

On est convaincu que ce qui favorise l'apprentissage, c'est le cadre dans lequel cela se passe plus que la préparation d'un·e format·eur·trice expert·e qui sait tout.

Nous apprécions les postures basses, nous préférons apprendre avec les participant·e·s plutôt qu'éduquer des stagiaires.

# C'est encore un peu brumeux pour moi, est-ce que vous pouvez m'aider à comprendre comment ça va se dérouler ?

Pas précisément, parce qu'on va construire le programme ensemble pendant le journée.

Nous avons une idée assez clair de ce que nous pourrions proposer, et nous allons adapter tout ça au contexte.

Par exemple, ce que nous pourrions faire à 4 ne ressemblerait pas à ce que nous ferrions à 12.

Pour t'aider à imaginer, on a compiler [quelques idées]().

#
